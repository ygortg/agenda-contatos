import java.util.*;
public class Agenda
{
    public static void main(String[] args)
    {
   
        Scanner lerTeclado = new Scanner(System.in);
        ArrayList<Contato> listaContatos = new ArrayList<Contato>();
        int opcao;
        do
        {
            System.out.println("\n ----------Esolha uma das opções:----------- \n ");
            System.out.println("1- Adicionar contato");
            System.out.println("2- Listar contato");
            System.out.println("3- Deletar contato");
            System.out.println("4- 0 para sair\n");
            opcao = lerTeclado.nextInt();
            lerTeclado.nextLine();
            switch(opcao)
            {
            case 1 :
               	Contato umContato= new Contato();
		 System.out.println("Digite um nome do contato");
                String umNome = lerTeclado.nextLine();
                umContato.setNome(umNome);
                System.out.println("Digite o telefone do contato");
                String umTelefone = lerTeclado.nextLine();
                umContato.setTelefone(umTelefone);
                System.out.println("Digite o sexo do contato(0-MASC ; 1-FEM)");
                int umSexo = lerTeclado.nextInt();
                umContato.setSexo(umSexo);
                if(umContato.getSexo() !=1 && umContato.getSexo() != 0)
                {
                    while(umContato.getSexo() !=1 && umContato.getSexo() != 0)
                    {
                        System.out.println("Sexo invalido");
                        System.out.println("Digite o sexo do contato(0-MASC ; 1-FEM)");
                        umSexo = lerTeclado.nextInt();
                        umContato.setSexo(umSexo);
                    }
                }

                listaContatos.add(umContato);
                System.out.println("Contato adicionado com sucesso");
                System.out.println("Nome: " +umContato.getNome());
                System.out.println("Telefone: "+umContato.getTelefone());
                if(umContato.getSexo() == 0)
                {
                    System.out.println("Sexo: "+umContato.getSexo()+"-MASCULINO");
                }
                if(umContato.getSexo() == 1)
                {
                    System.out.println("Sexo: "+umContato.getSexo()+"-FEMININO");
                }

                break;

            case 2:

                int tamanhoArray = listaContatos.size();
                int contador;
		for(contador=0; contador<tamanhoArray; contador++)
                {
                    System.out.println("\n -------Contato "+(contador+1)+"------- \n");
                    System.out.println("\nNome: "+listaContatos.get(contador).getNome());
                    System.out.println("\nTelefone: : "+listaContatos.get(contador).getTelefone());

                    if(listaContatos.get(contador).getSexo() == 0)
                    {
                        System.out.println("\nSexo: Masculino -  "+listaContatos.get(contador).getSexo()+"\n");
                    }
                    else if(listaContatos.get(contador).getSexo() == 1)
                    {
                        System.out.println("\nSexo: Feminino - "+listaContatos.get(contador).getSexo()+"\n");
                    }

                }
		break;
	   case 3:
		System.out.println("Qual contato deseja remover?(Digite a ordem do contato na lista) \n");
		contador = lerTeclado.nextInt();
		lerTeclado.nextLine();
		listaContatos.remove(contador-1);
		System.out.println("Contato removido");
            }
           

        } while(opcao!=0);
    }
}

